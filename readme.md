# Library

- jQuery v1.12.4
- Bootstrap v4.0.0
- Waypoints v4.0.1
- Owl Carousel v2.2.1
- animate.css v3.6.0
- Magnific Popup - v1.1.0
- nicescroll v3.7.6 
- Font Awesome Free 5.0.6
- Font Awesome Pro 5.7.2
- IcoFont v1.0.1
- MeanMenu 2.0.7 
- slick
- jQuery v1.12.4
- Isotope v3.0.5
- jQuery One Page Nav v3.0.0
- WOW - v1.1.3
- scrollup v2.4.1
- imagesLoaded v4.1.4
- parallax.js v1.4.2
- leaflet.js v1.5.1
- mail sending function

# installing software required
- rubyinstaller
- sass
- compass sass

# New Project 
compass create <projectName>

# Exiting Project
compass install compass
compass install susy
compass watch


# Loader Class Name List
```sh
<div class="clock"></div>
<div class="hourglass"></div>
<div class="loader1"></div>
<div class="loader2"></div>
<div class="loader3"></div>
<div class="loader4"></div>
<div class="loader5"></div>
<div class="loader6"></div>
<div class="loader6"></div>
<div class="loader8"></div>
<div class="loader9"></div>
<div class="loader10"></div>
```