 // Creating map options
 //var langLong = [40.6971494, -74.2598775];
 var langLong = [40.6971494, -74.2598775];
 var mapOptions = {
    center: langLong,
    zoom: 15
 }
 
 // Creating a map object
 var map = new L.map('map', mapOptions);
 
 // Creating a Layer object
 var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
 //var layer = new L.TileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png');
 //var googleLayer = new L.Google('ROADMAP');
 // Adding layer to the map
 map.addLayer(layer);
 // Icon options
 /* var iconOptions = {
    iconUrl: 'logo.png',
    iconSize: [50, 50]
 } */
 var iconOptions = {
    html: '<i class="fa fa-truck" style="color: red"></i>',
    iconSize: [20, 20],
 }
 // Creating a custom icon
 var customIcon = L.icon(iconOptions)
 // Creating a Marker
 var markerOptions = {
    title: "MyLocation",
    clickable: true,
    draggable: true,
    //icon: customIcon
    icon: L.divIcon({
        html: '<i class="far fa-map-marker-alt fa-5x" class="text-red"></i>',
        iconSize: [60, 60],
        className: 'mi-map-icon'
      })
 }
 var marker = L.marker(langLong,markerOptions);
 
 // Adding popup to the marker
 marker.bindPopup('This is Address').openPopup();
 // Adding marker to the map
 marker.addTo(map);

/* var latlngs = [
    [17.385044, 78.486671],
    [16.506174, 80.648015],
    [17.000538, 81.804034],
    [17.686816, 83.218482]
 ];
 // Creating a poly line
 var polyline = L.polyline(latlngs, {color: 'red'});
 
 // Adding to poly line to map
 polyline.addTo(map)*/